import numpy.random as rng
from keras import backend as K
from keras.layers import Input, Conv2D, Dense, Flatten, MaxPooling2D, Concatenate, Lambda
from keras.models import Model, Sequential
from keras.regularizers import l2

def euclidean_distance(vects):
    x, y = vects
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True) + 0.000001)


def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return shape1[0], 1

# The Regularizers are randomly Chosen

def sianet():
    input_shape = (224, 224, 3)
    left_input = Input(input_shape)
    right_input = Input(input_shape)
    convnet = Sequential()
    convnet.add(Conv2D(64, (10, 10), activation='relu', input_shape=input_shape, kernel_regularizer=l2(2e-4)))
    convnet.add(MaxPooling2D())
    convnet.add(Conv2D(128, (7, 7), activation='relu',kernel_regularizer=l2(2e-4)))
    convnet.add(MaxPooling2D())
    convnet.add(Conv2D(128, (4, 4), activation='relu', kernel_regularizer=l2(2e-4)))
    convnet.add(MaxPooling2D())
    convnet.add(Conv2D(256, (4, 4), activation='relu', kernel_regularizer=l2(2e-4)))
    convnet.add(Flatten())
    convnet.add(Dense(4096, activation="sigmoid", kernel_regularizer=l2(1e-3)))

    encoded_l = convnet(left_input)
    encoded_r = convnet(right_input)

    L1_distance = lambda x: K.abs(x[0] - x[1])

    merged = Concatenate(axis=1)([encoded_l, encoded_r])
    
    distance_layer = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape, name="distance")(merged)

    prediction = Dense(1, activation='sigmoid')(distance_layer)

    siamese_net = Model(input=[left_input, right_input], output=prediction)
    # optimizer = SGD(0.0004)
    return siamese_net
