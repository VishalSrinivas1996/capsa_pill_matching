import argparse
import os

import cv2.cv2 as cv2
import numpy as np
from keras import Model
from keras.applications import VGG19
from keras.applications import ResNet50
from keras.applications.imagenet_utils import preprocess_input

path = './Slope/Anomaly/'

model_VGG19 = VGG19(weights='imagenet', include_top=False)
model_Resnet = ResNet50(weights='imagenet', include_top=False)

for layer in model_Resnet.layers:
    print (layer.name, layer.output)



print("\nLoading pre-trained model...\n")

print("\nReading images from '{}' directory...\n".format(path))

final_features = []

for subdir, dirs, files in os.walk(path):
    for filename in files:
        if filename.endswith(".bmp"):
            filename_full = os.path.join(subdir, filename)
            img = cv2.imread(filename_full)
            img = np.expand_dims(img, axis=0)
            img = img.astype('float64')
            img = preprocess_input(img)
            features_vgg19 = model_VGG19.predict(img).flatten()
            features_resnet = model_Resnet.predict(img).flatten()
            features = np.concatenate((features_vgg19,features_resnet))
            print(features_vgg19.shape, features_resnet.shape, features.shape)
            final_features.append(features)

final_features = np.array(final_features)
print(final_features.shape)

np.save('./Slope/Anomaly_RESNET', final_features)
