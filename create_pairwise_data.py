import random

import numpy as np
from sklearn.model_selection import train_test_split


def load_data():
    pill_data = np.load('./Slope/npys/pill_data_2.npy')
    pill_label = np.load('./Slope/npys/pill_label_2.npy')
    x_train, x_test, y_train, y_test = train_test_split(pill_data, pill_label, test_size=0.33, random_state=42)
    return (x_train, y_train), (x_test, y_test)


def pair_generator_positive(positive_indices):
    used_pairs = set()

    while True:
        pair = random.sample(positive_indices, 2)
        pair = tuple(sorted(pair))
        if pair not in used_pairs:
            used_pairs.add(pair)
            yield pair

def pair_generator_negative(positive_indices, negative_indices):
    while True: 
        p_index = random.choice(positive_indices)
        n_index = random.choice(negative_indices)
        pair = tuple([p_index, n_index])
        yield pair

def create_pairs(pill_data, pill_labels):
    positive_pairs = []
    negative_pairs = []
    labels_indices = pill_labels.tolist()
    positive_indices = [i for i, x in enumerate(labels_indices) if x == 1]
    negative_indices = [i for i, x in enumerate(labels_indices) if x == 0]
    positive_gen = pair_generator_positive(positive_indices)
    negative_gen = pair_generator_negative(positive_indices, negative_indices)
    for i in range(500):
        pair = positive_gen.__next__()
        positive_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))

    for j in range(500):
        pair = negative_gen.__next__()
        negative_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))

    positive = np.array(positive_pairs)
    negative = np.array(negative_pairs)
    positive_labels = np.ones(positive.shape[0])
    negative_labels = np.zeros(negative.shape[0])
    pill_pair_label = np.hstack((positive_labels, negative_labels))
    pill_pair_data = np.vstack((positive, negative))
    print(pill_pair_data.shape, pill_pair_label.shape)
    idx = np.random.permutation(len(pill_pair_label))
    pill_pair_data_shuffled, pill_pair_label_shuffled = pill_pair_data[idx], pill_pair_label[idx]
    print(pill_pair_data_shuffled.shape, pill_pair_label_shuffled.shape)
    return pill_pair_data_shuffled, pill_pair_label_shuffled


(X_train, y_train), (X_test, y_test) = load_data()
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')

tr_pairs, tr_y = create_pairs(X_train, y_train)
print(tr_pairs[:, 0].shape)
print(tr_pairs[:, 1].shape)
