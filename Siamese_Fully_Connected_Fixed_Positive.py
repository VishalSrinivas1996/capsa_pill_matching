from __future__ import absolute_import
from __future__ import print_function

import numpy as np
from sklearn.model_selection import train_test_split

np.random.seed(1337)

import random
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, Lambda
from keras.optimizers import Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint



def load_data():
    pill_data = np.load('./Slope/npys/pill_data_RESNET.npy')
    pill_label = np.load('./Slope/npys/pill_label_RESNET.npy')
    print("Total Data Size: ", len(pill_label))
    x_train, x_test, y_train, y_test = train_test_split(pill_data, pill_label, test_size=0.20, random_state=42)
    print("Train Data size: ", len(y_train))
    print("Test Data Size: ", len(y_test))
    sample_feature = np.load('sample_feature_slope_crocin_VRESNET_5bmp.npy')
    return (x_train, y_train), (x_test, y_test), sample_feature


def euclidean_distance(vects):
    x, y = vects
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True) + 0.000001)


def distance_output_shape(shapes):
    shape1, shape2 = shapes
    return shape1[0], 1


def contrastive_loss(y_true, y_pred):
    margin = 1000
    return K.mean(y_true * K.square(y_pred) + (1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))


def create_pairs(pill_data, pill_labels, sample_feature):
    positive_pairs = []
    negative_pairs = []
    labels_indices = pill_labels.tolist()
    positive_indices = [i for i, x in enumerate(labels_indices) if x == 1]
    negative_indices = [i for i, x in enumerate(labels_indices) if x == 0]
    
    for i in positive_indices:
        positive_pairs.append(np.vstack((sample_feature, pill_data[i])))

    for j in negative_indices: 
        negative_pairs.append(np.vstack((sample_feature, pill_data[j])))

    positive = np.array(positive_pairs)
    negative = np.array(negative_pairs)
    
    positive_labels = np.ones(positive.shape[0])
    negative_labels = np.zeros(negative.shape[0])
    
    pill_pair_label = np.hstack((positive_labels, negative_labels))
    pill_pair_data = np.vstack((positive, negative))
    idx = np.random.permutation(len(pill_pair_label))
    pill_pair_data_shuffled, pill_pair_label_shuffled = pill_pair_data, pill_pair_label
    print(pill_pair_data_shuffled.shape, pill_pair_label_shuffled.shape)
    return pill_pair_data_shuffled, pill_pair_label_shuffled

def create_base_network(input_dim):
    model = Sequential()
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(128, activation='relu'))
    return model


def compute_accuracy(predictions, labels):
    return labels[predictions.ravel() < 1].mean()


(X_train, y_train), (X_test, y_test), sample_feature = load_data()
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
input_dim = 25088 + 2048
nb_epoch = 100

tr_pairs, tr_y = create_pairs(X_train, y_train, sample_feature)
te_pairs, te_y = create_pairs(X_test, y_test, sample_feature)

base_network = create_base_network(input_dim)

input_a = Input(shape=(input_dim,))
input_b = Input(shape=(input_dim,))

processed_a = base_network(input_a)
processed_b = base_network(input_b)

distance = Lambda(euclidean_distance, output_shape=distance_output_shape)([processed_a, processed_b])

model = Model(input=[input_a, input_b], output=distance)

adam = Adam(lr=5e-5)

filepath="crocin_fixed_positive_VRESNET_best_lr_5e5.h5"

checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
callbacks_list = [checkpoint]

model.compile(loss=contrastive_loss, optimizer=adam)
model.fit([tr_pairs[:, 0], tr_pairs[:, 1]], tr_y,
          validation_data=([te_pairs[:, 0], te_pairs[:, 1]], te_y),
          batch_size=32,                                                                                                                                                                                                                                                                                
          nb_epoch=nb_epoch,
          callbacks=callbacks_list, 
          verbose=1)

model.save('crocin_fixed_positive_VRESNET_lr_5e5.h5')

pred = model.predict([tr_pairs[:, 0], tr_pairs[:, 1]])
tr_acc = compute_accuracy(pred, tr_y)
pred = model.predict([te_pairs[:, 0], te_pairs[:, 1]])
te_acc = compute_accuracy(pred, te_y)                                                                                                                                                                                                                                                                                                                                                                                                                                           

print('Accuracy on training set: %0.2f%%' % (100 * tr_acc))
print('Accuracy on test set: %0.2f%%' % (100 * te_acc))
