import numpy as np

p0 = np.load('./Slope/npys/2_Advance.npy')
p1 = np.load('./Slope/Paracetamol.npy')
n = np.load('./Slope/Anomaly.npy')

p0_label = np.zeros(p0.shape[0])
p1_label = np.ones(p1.shape[0])
n_label = np.ones(n.shape[0]) * 2

q = np.vstack((p0, p1, n))
q_label = np.hstack((p0_label, p1_label, n_label))

print(p0.shape, p0_label.shape)
print(p1.shape, p1_label.shape)
print(n.shape, n_label.shape)
print(q.shape, q_label.shape)

np.save('pill_data_3', q)
np.save('pill_label_3', q_label)
