import cv2.cv2 as cv2 
import numpy as np
from keras import Model
from keras.models import load_model
from keras.applications import VGG19
from keras.applications.imagenet_utils import preprocess_input
import keras.backend as K


def contrastive_loss(y_true, y_pred):
    margin = 1
    return K.mean(y_true * K.square(y_pred) + (1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))


siamese = load_model('pill_simnet_conv.h5', custom_objects={'contrastive_loss': contrastive_loss})

image_sample = './Positive_Pills/pill438.bmp'
image_to_pred = './Positive_Pills/pill103.bmp'
# image_to_pred = './Negative_Pills/pill45323.bmp'

img_sample = cv2.imread(image_sample)
img_real = cv2.imread(image_to_pred)

img_sample = img_sample.reshape(1, 224, 224, 3)
img_real = img_real.reshape(1, 224, 224, 3)

pred = siamese.predict([img_sample, img_real])
print('prediction : {}'.format(pred[0][0]))
print('Distance : {}'.format(pred[1][0]))
