from __future__ import absolute_import
from __future__ import print_function

import numpy as np
from sklearn.model_selection import train_test_split

np.random.seed(1337)

import random
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, Lambda
from keras.optimizers import Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
from keras.models import load_model


def load_data():
    pill_data = np.load('./pill_data_3.npy')
    pill_label = np.load('./pill_label_3.npy')
    x_train, x_test, y_train, y_test = train_test_split(pill_data, pill_label, test_size=0.33, random_state=42)
    return (x_train, y_train), (x_test, y_test)


def euclidean_distance(vects):
    x, y = vects
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True) + 0.000001)


def distance_output_shape(shapes):
    shape1, shape2 = shapes
    return shape1[0], 1


def contrastive_loss(y_true, y_pred):
    margin = 1000
    return K.mean(y_true * K.square(y_pred) + (1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))


def pair_generator_positive(positive_indices):
    used_pairs = set()
    while True:
        pair = random.sample(positive_indices, 2)
        pair = tuple(sorted(pair))
        if pair not in used_pairs:
            used_pairs.add(pair)
            yield pair

def pair_generator_negative(positive_indices, negative_indices):
    used_pairs = set()
    while True: 
        p_index = random.choice(positive_indices)
        n_index = random.choice(negative_indices)
        pair = tuple(sorted([p_index, n_index]))
        if pair not in used_pairs:
            used_pairs.add(pair)
            yield pair

def create_pairs(pill_data, pill_labels, samples):
    positive_pairs = []
    negative_pairs = []
    labels_indices = pill_labels.tolist()

    data_index = [None, None, None]

    print ("Filling data_index\n")
    for index in range (3):
        data_index[index] = [i for i, x in enumerate(labels_indices) if x == index]
    
    # positive_indices = [i for i, x in enumerate(labels_indices) if x == 1]
    # negative_indices = [i for i, x in enumerate(labels_indices) if x == 0]
    
    print ("pair_generator_positive\n")
    positive_gen_0 = pair_generator_positive(data_index[0])
    positive_gen_1 = pair_generator_positive(data_index[1])

    print ("pair_generator_negative\n")
    negative_gen_0 = pair_generator_negative(data_index[0], data_index[1])
    negative_gen_1 = pair_generator_negative(data_index[0], data_index[2])
    negative_gen_2 = pair_generator_negative(data_index[1], data_index[2])
    
    print ("Positive 0\n")
    for i in range(samples//(2*2) ):
        pair = positive_gen_0.__next__()
        positive_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))
    
    print ("Positive 1\n")
    for i in range(samples//(2*2) ):
        pair = positive_gen_1.__next__()
        positive_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))

    print ("Negative 0\n")
    for j in range(samples//(2*3)):
        pair = negative_gen_0.__next__()
        negative_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))
    
    print ("Negative 1\n")
    for j in range(samples//(2*3)):
        pair = negative_gen_1.__next__()
        negative_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))
    
    print ("Negative 2\n")
    for j in range(samples//(2*3)):
        pair = negative_gen_2.__next__()
        negative_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))


    positive = np.array(positive_pairs)
    negative = np.array(negative_pairs)
    positive_labels = np.ones(positive.shape[0])
    negative_labels = np.zeros(negative.shape[0])
    pill_pair_label = np.hstack((positive_labels, negative_labels))
    pill_pair_data = np.vstack((positive, negative))
    idx = np.random.permutation(len(pill_pair_label))
    pill_pair_data_shuffled, pill_pair_label_shuffled = pill_pair_data[idx], pill_pair_label[idx]
    print(pill_pair_data_shuffled.shape, pill_pair_label_shuffled.shape)
    return pill_pair_data_shuffled, pill_pair_label_shuffled

def create_base_network(input_dim):
    model = Sequential()
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(128, activation='relu'))
    return model


def compute_accuracy(predictions, labels):
    return labels[predictions.ravel() < 1].mean()


(X_train, y_train), (X_test, y_test) = load_data()
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
input_dim = 25088
nb_epoch = 100

print ("Creating Pairs for training....")
tr_pairs, tr_y = create_pairs(X_train, y_train, 10000)
print ("Done\n")
print ("Creating Pairs for Testing....")
te_pairs, te_y = create_pairs(X_test, y_test, 1000)
print ("Done\n")

base_network = create_base_network(input_dim)

input_a = Input(shape=(input_dim,))
input_b = Input(shape=(input_dim,))

processed_a = base_network(input_a)
processed_b = base_network(input_b)

distance = Lambda(euclidean_distance, output_shape=distance_output_shape)([processed_a, processed_b])

if 1:
    print("Constructed model")
    model = Model(input=[input_a, input_b], output=distance)
else:
    model = load_model ("old_pill_siamese_best_proper.h5", custom_objects={'contrastive_loss': contrastive_loss})




adam = Adam(5e-6)

filepath="2_positives_siamese_margin_1000_best_dropout_0_25.h5"
filepath_1='2_positives_siamese_margin_1000_dropout_0_25.{epoch:02d}-{val_loss:.2f}.h5'
checkpoint_1 = ModelCheckpoint(filepath_1, monitor='val_loss', verbose=1, save_best_only=False, mode='min', period=10)
checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
callbacks_list = [checkpoint, checkpoint_1]

model.compile(loss=contrastive_loss, optimizer=adam)
model.fit([tr_pairs[:, 0], tr_pairs[:, 1]], tr_y,
          validation_data=([te_pairs[:, 0], te_pairs[:, 1]], te_y),
          batch_size=16,                                                                                                                                                                                                                                                                                
          nb_epoch=nb_epoch,
          callbacks=callbacks_list, 
          verbose=1)

pred = model.predict([tr_pairs[:, 0], tr_pairs[:, 1]])
tr_acc = compute_accuracy(pred, tr_y)
pred = model.predict([te_pairs[:, 0], te_pairs[:, 1]])
te_acc = compute_accuracy(pred, te_y)                                                                                                                                                                                                                                                                                                                                                                                                                                           

print('Accuracy on training set: %0.2f%%' % (100 * tr_acc))
print('Accuracy on test set: %0.2f%%' % (100 * te_acc))
