from keras.optimizers import Adam

import SiaNet
from SiaUtils import SimNet_Utilies
import numpy as np

def compute_accuracy(predictions, labels):
    return labels[predictions.ravel() < 0.5].mean()


optimizer = Adam(0.00006)

siamese_net = SiaNet.sianet()

siamese_net.compile(loss="binary_crossentropy", optimizer=optimizer)

siamese_net.count_params()

pill_train = np.load('pill_data_1.npy')
pill_val = np.load('pill_label_1.npy')

loader = SimNet_Utilies(pill_train, pill_val)

tr_pairs, tr_y = loader.create_pairs(500)
te_pairs, te_y = loader.create_pairs(50)

nb_epoch = 10

optimizer = Adam()

siamese_net.compile(loss='binary_cross_entropy', optimizer=optimizer, metrics=metrics.binary_accuracy)
siamese_net.fit([tr_pairs[:, :224], tr_pairs[:, 224:]], [tr_y, tr_y],
            validation_data=([te_pairs[:, :224], te_pairs[:, 224:]], [te_y, te_y]),
            batch_size=10,
            nb_epoch=nb_epoch)

siamese_net.save('pill_simnet_conv.h5')

pred = siamese_net.predict([tr_pairs[:, :224], tr_pairs[:, 224:]])
tr_acc = compute_accuracy(pred, tr_y)
pred = siamese_net.predict([te_pairs[:, :224], te_pairs[:, 224:]])
te_acc = compute_accuracy(pred, te_y)

print('* Accuracy on training set: %0.2f%%' % (100 * tr_acc))
print('* Accuracy on test set: %0.2f%%' % (100 * te_acc))
